#!/usr/bin/env python3


from edition.position.IMovable import Movable
from draw.basics.Point import Point


class EnvironmentalObject(Movable):

    __origin = None

    def __init__(self, x, y, z=0):
        self.__origin = Point(x, y, z)

    @Movable.__move__()
    def __move__(self, x, y, z=0):
        self.__origin.__move__(x, y, z)
