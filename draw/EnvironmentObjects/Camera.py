#!/usr/bin/env python3


from .Environmental_object import EnvironmentalObject


class Camera(EnvironmentalObject):

    def __init__(self, x, y, z=0):
        super().__init__(x, y, z)
