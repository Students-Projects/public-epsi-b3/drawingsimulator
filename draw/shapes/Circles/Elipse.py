#!/usr/bin/env python3

from draw.shapes.shape import shape
from math import pi
from draw.basics.Point import Point


class Elipse(shape):
    __x_ray = 0
    __y_ray = 0

    def __init__(self, width, height, x, y, z=0):
        super().__init__()
        self.__x_ray = width
        self.__y_ray = height
        self.__origin_point = Point(x, y, z)

    @shape.__surfaceCalc__
    def surfaceCalc(self):
        return self.__x_ray * self.__y_ray*pi

    @shape.__draw__
    def __draw__(self):
        pass

    def set_axe_x(self, x):
        self.__x_ray = x

    def set_axe_y(self, y):
        self.__y_ray = y
