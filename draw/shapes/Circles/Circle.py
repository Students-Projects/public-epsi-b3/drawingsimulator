#!/usr/bin/env python3.7

from .Elipse import Elipse
from edition.color.Color import Color


class Circle(Elipse):
    def __init__(self, rayon, center):
        super().__init__(rayon, rayon, center)

