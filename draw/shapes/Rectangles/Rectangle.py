#!/usr/bin/env python3.7

from draw.shapes.shape import shape
from draw.basics.Point import Point


class Rectanlge(shape):
    __width = 0
    __height = 0

    __A = Point(0, 0, 0)
    __B = Point(0, 0, 0)
    __C = Point(0, 0, 0)
    __D = Point(0, 0, 0)

    def __init__(self, width, height, x, y, z):
        super().__init__(x, y, z)
        self.__width = width
        self.__height = height

    @shape.__surfaceCalc__
    def __surfaceCalc__(self):
        return self.__width*self.__height

    @shape.__draw__
    def __draw__(self):
        self.__A.__draw__()
        self.__B.__draw__()
        self.__C.__draw__()
        self.__D.__draw__()
        pass

    def set_width(self, width):
        self.__width = width

    def set_height(self, height):
        self.__height = height

    @shape.__move__
    def __move__(self, x, y, z=0):
        super().__move__(x, y, z)


