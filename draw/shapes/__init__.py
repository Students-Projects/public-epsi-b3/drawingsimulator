#!/usr/bin/env python3.7

from .shape import shape
from .Rectangles import *
from .Circles import *
