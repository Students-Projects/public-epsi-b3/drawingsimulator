#!/usr/bin/env python3


from draw.basics.Point import Point
from edition.color.Color import Color
from ..Idrawable import Drawable
from edition.position.IMovable import Movable
from edition.color.Icolorable import Colorable


class shape(Drawable, Movable, Colorable):
    # Colors in RGB
    __color = Color()
    __origin_point = Point(0, 0, 0)

    def __init__(self, x=0, y=0, z=0):
        self.__origin_point = Point(x, y, z)

    @Drawable.__draw__
    def __draw__(self):
        pass

    def __surfaceCalc__(self):
        pass

    def __getOrigin__(self):
        return self.__origin_point

    @Movable.__move__()
    def __move__(self, x, y, z=0):
        self.__origin_point.__move__(x, y, z)

    @Colorable.fill()
    def fill(self, color, options=None):
        pass

    @Colorable.stroke_color()
    def stroke_color(self, color, options=None):
        pass


