#!/usr/bin/env python3

from .Point import Point
from math import sqrt, pow
from edition.position.IMovable import Movable


class Line(Movable):
    __origin = Point(0, 0, 0)
    __endPoint = Point(0, 0, 0)

    def __init__(self, ox=None, oy=None, oz=None, ex=None, ey=None, ez=None, origin=None, endpoint=None):
        if None not in {ox, oy, oz, ex, ez, ez}:
            self.__origin = Point(ox, oy, oz)
            self.__endPoint = Point(ex, ey, ez)
            self.__origin = origin
            self.__endPoint = endpoint
        else:
            if None in {origin, endpoint}:
                raise Exception("Shit")
            else:
                self.__origin = origin
                self.__endPoint = endpoint

    def get_length(self):
        # sqrt ( (xb - xa)²+(yb-ya)²+(zb-za)² )
        norme_x = self.__endPoint.get_x - self.__origin.get_x
        norme_y = self.__endPoint.get_y - self.__origin.get_y
        norme_z = self.__endPoint.get_z - self.__origin.get_z

        return sqrt(pow(norme_x, 2)+pow(norme_y, 2)+pow(norme_z, 2))

    @Movable.__move__()
    def __move__(self, x, y, z=0):
        self.__origin.move(x, y, z)
        self.__endPoint.move(x, y, z)
        pass

