#!/usr/bin/env python3

from edition.position.IMovable import Movable
from draw.Idrawable import Drawable


class Point(Movable, Drawable):

    __x = 0  # abscissa
    __y = 0  # Ordinate
    __z = 0  # Vertical

    def __init__(self, x, y, z=0):
        self.__x = x
        self.__y = y
        self.__z = z

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_z(self):
        return self.__z

    @Drawable.__draw__
    def __draw__(self):
        print("A new point is existing at coordonate: ("+str(self.__x)+","+str(self.__y)+","+str(self.__z)+") .")
        pass

    @Movable.__move__()
    def __move__(self, x, y, z=0):
        self.__x += x
        self.__y += y
        self.__z += z
        pass
