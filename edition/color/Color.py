#!/usr/bin/env python3.7


class Color:
    __red = 255
    __green = 255
    __blue = 255

    def __init__(self, r=255, g=255, b=255):
        self.rgb(r, g, b)

    def rgb(self, r, g, b):
        self.__red = r
        self.__green = g
        self.__blue = b

    def set_blue(self,b):
        self.__blue = b

    def set_red(self, r):
        self.__red = r

    def set_green(self, g):
        self.__green = g

    def get_blue(self,b):
        return self.__blue

    def get_red(self, r):
        return self.__red

    def get_green(self, g):
        return self.__green

